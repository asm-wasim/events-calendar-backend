# Events Calendar

A simple events calendar,

## Includes

- APIs for viewing, adding, and updating events
- Necessary database tables for storing events
- Token-based authentication for users
- Standard Pagination class for paginating the events

## Limitations

- Event occurrences are generating on the fly, which is not efficient for large amounts of events
- Unnecessary queries are for past events
- No update support for recurring events
- Unit tests are not included

## Plans for optimization

- Use a separate table for storing event occurrences
- Pre-generate event occurrences for a certain period
- Generate event occurrences in the background using scheduled tasks
- Apply caching

## Getting Started

Clone the repository with SSH:

```bash
git clone git@gitlab.com:asm-wasim/events-calendar-backend.git
```

Or with HTTPS:

```bash
git clone https://gitlab.com/asm-wasim/events-calendar-backend.git
```

Run the following commands:

```bash
cd events-calendar-backend
pip install -r requirements.txt
```

Create a `.env` file in the root directory and add the following:

```env
ENV_TYPE = 'DEVELOPMENT' or 'PRODUCTION' or 'STAGING'
SECRET_KEY =your_secret_key
```

SQLite3 is used as the development database. To create the database, run the following command:

```bash
python manage.py migrate
```

To run the server, run the following command:

```bash
python manage.py runserver
```

## API Endpoints

- `/api/event/v1/events/` - GET
- `/api/event/v1/events/create/` - POST
- `/api/event/v1/events/<int:pk>/update/` - PUT, PATCH

## Pagination

- `/api/event/v1/events/?page=1&&page_size=10` - GET


