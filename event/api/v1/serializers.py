from collections import defaultdict

from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from event.models import Event


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = [
            'id',
            'title',
            'description',
            'all_day',
            'start_at',
            'end_at',
            'recurring',
            'recurring_type',
            'recurring_interval',
            'by_weekdays',
            'recurring_until',
            'occurrence',
        ]

    def validate(self, attrs):
        validation_errors = defaultdict(list)

        all_day = attrs.get('all_day')
        start_at = attrs.get('start_at')
        end_at = attrs.get('end_at', None)

        if not all_day:
            if not end_at:
                validation_errors['end_at'].append('This field is required.')
            elif start_at > end_at:
                validation_errors['start_at'].append('Start date and time cannot be greater than end date and time.')

        recurring = attrs.get('recurring')
        if recurring:
            if not attrs.get('recurring_type', None):
                validation_errors['recurring_type'].append('Recurring type is required for recurring events.')
            if attrs.get('recurring_until') and attrs.get('occurrence'):
                validation_errors['recurring_until'].append('Recurring until and occurrence are mutually exclusive.')
            if attrs.get('recurring_until') and attrs.get('recurring_until') < start_at:
                validation_errors['recurring_until'].append('Recurring until cannot be before the start date.')
            if attrs.get('occurrence') and attrs.get('occurrence') < 1:
                validation_errors['occurrence'].append('Occurrence should be a positive integer.')

        if validation_errors:
            raise serializers.ValidationError({'errors': validation_errors})
        return attrs


class EventUpdateSerializer(EventSerializer):
    edit_type = serializers.ChoiceField(
        label=_('Edit Type'),
        help_text=_('Type of edit to be performed on the event.'),
        write_only=True,
        required=False,
        choices=[
            ('single', 'Single'),
            ('this_and_future', 'This and Future'),
            ('all', 'All'),
        ],
    )

    class Meta(EventSerializer.Meta):
        fields = EventSerializer.Meta.fields + ['edit_type']

    def validate(self, attrs):
        attrs = super().validate(attrs)
        edit_type = attrs.get('edit_type', None)
        recurring = attrs.get('recurring')

        if recurring:
            if not edit_type:
                raise serializers.ValidationError({
                    'errors': {'edit_type': ['Edit type is required for recurring events.']}})
            if edit_type == 'single':
                raise serializers.ValidationError({
                    'errors': {'edit_type': ['Single edit type is currently not supported.']}})

        return attrs

    def update(self, instance, validated_data):
        recurring = validated_data.get('recurring')
        if not recurring:
            return super().update(instance, validated_data)

        # TODO: Implement recurring event update logic
