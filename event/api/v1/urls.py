from django.urls import path

from event.api.v1.views import EventCreateAPIView, EventListAPIView, EventUpdateAPIView

app_name = 'v1'

urlpatterns = [
    path(route='events/', view=EventListAPIView.as_view(), name='event-list'),
    path(route='events/create', view=EventCreateAPIView.as_view(), name='event-create'),
    path(route='events/<int:pk>/update', view=EventUpdateAPIView.as_view(), name='event-update'),
]
