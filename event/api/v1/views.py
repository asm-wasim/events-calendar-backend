from datetime import datetime

from django.utils import timezone
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from core.pagination import StandardResultsSetPagination
from event.api.v1.serializers import EventSerializer, EventUpdateSerializer
from event.models import Event


class EventCreateAPIView(CreateAPIView):
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class EventUpdateAPIView(RetrieveUpdateAPIView):
    serializer_class = EventUpdateSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Event.objects.filter(user=self.request.user)


class EventListAPIView(APIView):
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = StandardResultsSetPagination

    def get(self, request, *args, **kwargs):
        start_datetime = self.request.query_params.get('start_date', None)
        end_datetime = self.request.query_params.get('end_date', None)

        if not start_datetime or not end_datetime:
            current_datetime = timezone.now()
            week_start = current_datetime - timezone.timedelta(days=current_datetime.weekday())
            week_end = week_start + timezone.timedelta(days=6)
            start_datetime = week_start.replace(hour=0, minute=0, second=0, microsecond=0)
            end_datetime = week_end.replace(hour=23, minute=59, second=59, microsecond=999999)
        else:
            start_datetime = timezone.make_aware(datetime.strptime(start_datetime, '%Y-%m-%dT%H:%M:%S.%fZ'))
            end_datetime = timezone.make_aware(datetime.strptime(end_datetime, '%Y-%m-%dT%H:%M:%S.%fZ'))

        events = Event.objects.filter(user=self.request.user)
        events_with_occurrences = []
        for event in events:
            occurrences = event.get_occurrences(start_datetime, end_datetime)
            events_with_occurrences.extend(occurrences)

        paginator = self.pagination_class()
        result_page = paginator.paginate_queryset(events_with_occurrences, request)
        serializer = self.serializer_class(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)
