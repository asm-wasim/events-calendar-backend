from django.urls import include, path

app_name = 'event'

urlpatterns = [
    path('v1/', include('event.api.v1.urls')),
]
