from dateutil.rrule import *
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _


class RecurringTypeChoices(models.TextChoices):
    DAILY = "daily", _("Daily")
    WEEKLY = "weekly", _("Weekly")
    MONTHLY = "monthly", _("Monthly")
    YEARLY = "yearly", _("Yearly")


class Event(models.Model):
    user = models.ForeignKey(
        verbose_name=_("User"),
        to="accounts.User",
        on_delete=models.PROTECT,
        related_name="events"
    )

    title = models.CharField(verbose_name=_("Title"), max_length=255)
    description = models.TextField(verbose_name=_("Description"), blank=True)

    all_day = models.BooleanField(verbose_name=_("All Day"), default=False)
    start_at = models.DateTimeField(verbose_name=_("Start At"))
    end_at = models.DateTimeField(verbose_name=_("End At"), blank=True)

    recurring = models.BooleanField(verbose_name=_("Recurring"), default=False)
    recurring_type = models.CharField(
        verbose_name=_("Recurring Frequency"),
        max_length=31,
        choices=RecurringTypeChoices.choices,
        blank=True
    )
    recurring_interval = models.PositiveIntegerField(verbose_name=_("Recurring Interval"), default=1)
    by_weekdays = models.JSONField(verbose_name=_("By Weekdays"), default=list, blank=True)
    recurring_until = models.DateTimeField(verbose_name=_("Recurring Until"), null=True, blank=True)
    occurrence = models.PositiveIntegerField(verbose_name=_("Occurrence"), null=True, blank=True)

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")

    def clean(self):
        if self.end_at and self.start_at > self.end_at:
            raise ValidationError(_("Start date and time cannot be greater than end date and time."))

        if self.recurring and not self.recurring_type:
            raise ValidationError(_("Recurring type is required for recurring events."))

        if self.recurring_until and self.occurrence:
            raise ValidationError(_("Recurring until and occurrence are mutually exclusive."))

        if self.recurring_until and self.recurring_until < self.start_at:
            raise ValidationError(_("Recurring until cannot be before the start date."))

        if self.occurrence and self.occurrence < 1:
            raise ValidationError(_("Occurrence should be a positive integer."))

    def get_recurring_rule_params(self):
        _rules = {}
        if self.recurring_until:
            _rules['until'] = self.recurring_until
        if self.occurrence:
            _rules['count'] = self.occurrence
        return _rules

    def get_recurrence_rule(self):
        """ Returns a recurrence rule object based on the recurring type of the event. """
        if self.recurring:
            return rrule(
                freq=eval(self.recurring_type.upper()),
                dtstart=self.start_at,
                wkst=SU,
                interval=self.recurring_interval,
                **self.get_recurring_rule_params()
            )

        return None

    def get_occurrences(self, start_datetime, end_datetime):
        """ Accepts start and end date objects and returns a list of Event objects that fall between the start and
        end date arguments, representing occurrences of this particular event. """

        if not self.recurring:
            return [self] if start_datetime <= self.start_at <= end_datetime else []

        rule = self.get_recurrence_rule()
        recurrence_dates = rule.between(start_datetime, end_datetime, inc=True)
        duration = self.end_at - self.start_at
        events = [
            Event(
                id=self.id,
                user=self.user,
                title=self.title,
                description=self.description,
                all_day=self.all_day,
                start_at=date,
                end_at=date + duration,
                recurring=self.recurring,
                recurring_type=self.recurring_type,
                recurring_interval=self.recurring_interval,
                by_weekdays=self.by_weekdays,
                recurring_until=self.recurring_until,
                occurrence=self.occurrence
            ) for idx, date in enumerate(recurrence_dates)
        ]
        return events
