from django.contrib import admin

from event.models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_filter = [
        'all_day',
        'recurring',
        'recurring_type',
    ]
    list_display = [
        'id',
        'title',
        'user',
        'all_day',
        'start_at',
        'end_at',
        'recurring',
        'recurring_type',
        'recurring_interval',
        'recurring_until',
        'occurrence',
    ]
    list_display_links = [
        'id',
        'title',
    ]
    search_fields = [
        'title',
        'description',
    ]
    date_hierarchy = 'start_at'
    fieldsets = [
        (None, {'fields': ['user', 'title', 'description']}),
        ('Date & Time', {'fields': ['all_day', 'start_at', 'end_at']}),
        (
            'Recurring',
            {'fields': [
                'recurring',
                'recurring_type',
                'recurring_interval',
                'by_weekdays',
                'recurring_until',
                'occurrence'
            ]}
        ),
    ]
