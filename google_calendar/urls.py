from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.urls.conf import include

from google_calendar.settings import MEDIA_ROOT, MEDIA_URL, STATIC_ROOT, STATIC_URL, env

api_urlpatterns = [
    path('event/', include('event.api.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api_auth/', include('rest_framework.urls')),
    path('api/', include(api_urlpatterns)),
]

if env.str('ENV_TYPE') == 'DEVELOPMENT':
    urlpatterns += static(STATIC_URL, document_root=STATIC_ROOT)
    urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
